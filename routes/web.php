<?php
use App\User;
use App\Role;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/create', function(){

    $user = User::find(1);
    $role = new Role(['name'=>'Author']);

    $user->roles()->save($role);
    
});

Route::get('/read', function(){
   
    $user = User::findOrFail(1);
    foreach ($user->roles as $role) {

        echo $role->name;

    }
    //dd($user->roles);
});

Route::get('/update', function(){

    $user = User::findOrFail(1);
    
    if($user->has('roles')){
        foreach ($user->roles as $role) {

            if($role->name = 'Administrator');

            $role->name = 'subs';
            $role->save();
        }
    }
});

Route::get('/delete', function(){

    $user = User::findOrFail(1);

    foreach ($user->roles as $role){

        $role->whereId(5)->delete();
    }

    //$user->roles()->delete();

});

Route::get('/attach',function(){

    $user = User::findOrFail(1);

    $user->roles()->attach(6);


});

Route::get('/detach',function(){
    $user = User::findOrFail(1);
    $user->roles()->detach(6);
});


Route::get('/sync', function(){

    $user = User::findOrFail(1);
    $user->roles()->sync([7]);
});

